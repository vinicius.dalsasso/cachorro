package br.com.proway.cachorro;

import br.com.proway.cachorro.model.Cachorro;

import javax.swing.*;

import static javax.swing.JOptionPane.*;

public class Main {
    public static void main(String[] args){


        Cachorro[] cachorros = new Cachorro[3];

        for(int i =0; i < cachorros.length; i++){
            cachorros[i] = new Cachorro();
            cachorros[i].id = i + 1;
            cachorros[i].nome = showInputDialog(null, "digite o nome do cachorro " + cachorros[i].id );
            cachorros[i].raca = showInputDialog(null, "digite a raca do cachorro: ");
            cachorros[i].idade = Integer.parseInt(showInputDialog(null, "digite a idade do cachorro: "));

            JOptionPane.showMessageDialog(null, "O nome do cachorro " + cachorros[i].id + " é: " + cachorros[i].nome);
            JOptionPane.showMessageDialog(null, "A raça do cachorro " + cachorros[i].id  + " é: " + cachorros[i].raca);
            JOptionPane.showMessageDialog(null, "A idade do cachorro " + cachorros[i].id + " é: " + cachorros[i].idade);
        }

//         for(int i =0; i < cachorros.length; i++){
//            JOptionPane.showMessageDialog(null, cachorros[i].nome + cachorros[i].idade);
//         }
    }
}